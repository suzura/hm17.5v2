﻿

#include <iostream>

class Vector
{
private:

	double x;
	double y;
	double z;

public:
	Vector() : x(0), y(0), z(0)
	{}
	Vector(double _x, double _y, double _z) : x(_x), y(_y), z(_z)
	{}
	
	void ShowVector()
	{
		std::cout << '\n' << x << ' ' << y << ' ' << z << '\n';
	}

	void ModulVectora()
	{	
		std::cout << sqrt(pow(x, 2) + pow(y, 2) + pow(z, 2)) << '\n';
	}

};

int main()
{
	Vector v(7,7,4);
	v.ShowVector();
	v.ModulVectora();
}

